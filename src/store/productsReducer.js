const defaultState =  []

const FETCH_PRODUCTS = "FETCH_PRODUCTS";

export const productsReducer = (state=defaultState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS:
            return action.payload

        default:
            return state
    }
} 

export const fetchProductsAction = (payload) => ({type: FETCH_PRODUCTS, payload})