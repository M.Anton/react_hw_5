const DELETE_ALL_PRODUCTS_IN_CART = "DELETE_ALL_PRODUCTS_IN_CART"

let defaultState = []

if (localStorage.getItem("addedToCartProducts")) {
    defaultState = JSON.parse(localStorage.getItem("addedToCartProducts"))
}

export const productsInCartReducer = (state=defaultState, action) => {
    switch (action.type) {
        case DELETE_ALL_PRODUCTS_IN_CART:
            localStorage.removeItem("addedToCartProducts")
            return state;

        default:
            return state;
    }
}

export const deleteAllProductsInCartAction = () => ({type: DELETE_ALL_PRODUCTS_IN_CART})